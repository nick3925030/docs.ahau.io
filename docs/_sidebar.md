  
  - [Setup Pātaka](pataka-guide.md?)
  - [Setup Āhau](download_guide.md)
  - [Getting Started](get_started.md)
  - [Create a Tribe](community_guide.md)
  - [Create a Story](records_guide.md)
  - [Create a Genealogy Record](whakapapa_guide.md)
  - [Create a Collection](collections_guide.md)
  - [Join your tribe](joining_guide.md)
