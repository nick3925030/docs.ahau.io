# Create a Tribe

In Āhau a Tribe is a private group with its own community profile and archive and encryption keys. Community members will be able to access and share information and records. Whakapapa records, profiles and stories created in the tribal archive will be accessible by approved tribal members. Tribes are similar to private groups in social media platforms, except all information is encrypted with a secret tribal key and can only be accessed by tribal members who have been approved and given a copy of the key.  

- To get started click on the *'+'* button in the top navigation bar.

<img src="_media/create_community_1.png" alt="New tribe button" width="800"/>

- You'll be navigated to the Tribes page.

<img src="_media/create_community_2.png" alt="tribes page" width="800"/>

- Click on the *'+ new tribe'* button in the top right corner. A pop up window will appear.


## Add your Tribes profile information

- Fill in all of the details for creating your new Tribe, including a Tribe name, description of the Tribe and its purpose, and relevant contact details. 
- Upload an image to reflect your Tribe, e.g. a logo. 
- click *'next'* to setup your community data model

<img src="_media/create_community_3.png" alt="New tribe popup" width="800"/>


## Setup your community profiles data model

On this page you can create your community data model. These are the profile fields that will be available about on community members and genealogy profiles.

- First you will see a list of the default profile fields.
- If you dont want these fields you can delete them by clicking the *'rubbish bin icon'*

If you require any of these fields to be filled by community members when they join a community your community 
- you can tick the *'required'* check box. 

This will not let them submit a request to join your tribe without inputting some information into this field on their profile.

- If you scroll to the bottom you will also see the option to *'+ add data point'*. 

This where you can add any other fields that you would like to record about your community members. Examples maybe be a social media links, shareholder numbers, or marae connections.

<img src="_media/create_community_5.png" alt="Tribe data model" width="800"/>

Once you are happy with your community profiles data model you can now add some questions that you would like community members to answer when requesting to join your Tribe.

- When your ready click *'next'* to move on to your Tribe settings. 


## Select your community features

Here you can select which features you want in your community archive.

By default all features are enabled but if there is a feature that you do not want to use for instance the whakapapa (genealogy) records of the peoples list (tribal registry) you can turn them off here.


Kaitiaki Features are features that are only available to Tribe administrators (Kaitiaki) 

Members Features are features that will be available to all community members

All of these settings can be adjusted/enabled or disabled at anytime by visiting your community profile settings. 

This is also where you will also be able to upgarde community members to Kaitiaki once you have other members in your Tribe. 

<img src="_media/create_community_6.png" alt="Tribe settings" width="800"/>

- When your ready, click the 'save' button to save your tribe. 

### You have successfully created a new Tribe

You have successfully created a Tribe. Your Tribe profile details can be edited at any time by the Kaitiaki (creator/administrator) of the Tribe.

<img src="_media/create_community_7.png" alt="Tribe profile page" width="800"/>

You might want to try creating a 
  - [create a record for tribal archive](records_guide)
  - [create a whakapapa record](whakapapa_guide)

If you have any pātai or run into any problems, please get in touch with us at [info@ahau.io](mailto:info@ahau.io) or [chat.ahau.io](https://chat.ahau.io/channel/help). 