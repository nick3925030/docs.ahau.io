# docs.ahau.io

gitlab pages + docsify!

docs:
- docsify: https://docsify.js.org
- gitlab pages : https://docs.gitlab.com/ee/user/project/pages/


## Editing

install something which can serve the docs folder

e.g. 

```bash
npm install -g serve
serve docs
```

Then open localhost:5000/ in your web browser

Edit the files, then push reload

## Deploy

Commit your changes to git and push them up to the master branch

`.gitlab-ci.yml` config takes care of automatically deploying
